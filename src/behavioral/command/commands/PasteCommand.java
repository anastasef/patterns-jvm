package behavioral.command.commands;

import behavioral.command.editor.Editor;

public class PasteCommand extends Command {

    public PasteCommand(Editor editor) {
        super(editor);
    }

    @Override
    public boolean execute() {
        if (editor.clipboard == null || editor.clipboard.isEmpty()) return false;

        backup(); // We need a backup to undo the changes
        editor.textField.insert(editor.clipboard, editor.textField.getCaretPosition());

        return true; // Undo is enabled
    }
}
