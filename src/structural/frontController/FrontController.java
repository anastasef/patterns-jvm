package structural.frontController;

public class FrontController {
    private Dispatcher dispatcher;

    public FrontController() {
        dispatcher = new Dispatcher();
    }

    private boolean isAuthenticUser() {
        System.out.println("FrontController: User is authenticated successfully");
        return true;
    }

    private void trackRequest(String request) {
        System.out.println("FrontController: Page requested: " + request);
    }

    public void dispatchRequest(String request) {
        // Log each request
        trackRequest(request);

        // Authenticate the user
        if (isAuthenticUser()) {
            dispatcher.dispatch(request);
        }
    }
}
