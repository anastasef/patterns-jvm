# Java Patterns

Examples of patterns implementations

## Classification of patterns

- **Creational patterns** provide object creation mechanisms that increase flexibility and reuse of existing code. 
- **Structural patterns** explain how to assemble objects and classes into larger structures, while keeping the structures flexible and efficient.
- **Behavioral patterns** take care of effective communication and the assignment of responsibilities between objects.

---

## Links

- [Справочник по паттернам](http://design-pattern.ru/index.html)
- [Core J2EE Patterns](http://www.corej2eepatterns.com/)